json.array!(@donations) do |donation|
  json.extract! donation, :id, :challenge_id, :user_id, :event_id, :donation_amount
  json.url donation_url(donation, format: :json)
end
