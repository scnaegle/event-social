json.array!(@flagged_items) do |flagged_item|
  json.extract! flagged_item, :id, :item_id, :item_type, :reason, :user_id, :moderator_id, :flagged
  json.url flagged_item_url(flagged_item, format: :json)
end
