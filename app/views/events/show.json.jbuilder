json.extract! @event, :id, :title, :description, :start_date, :end_date, :user_id, :cancelled_flag, :completed_flag, :created_at, :updated_at
