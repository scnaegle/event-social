json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :title, :description, :end_date, :user_id
  json.url challenge_url(challenge, format: :json)
end
