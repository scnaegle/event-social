class Challenge < ActiveRecord::Base
  belongs_to :user
  has_many :submissions
  has_many :comments
  has_many :donations
  has_many :charities, through: :donations
  has_many :flags, as: :item, class_name: FlaggedItem
  has_one :location, as: :addressable, class_name: Address

  accepts_nested_attributes_for :location, reject_if: :all_blank, allow_destroy: true

  enum challenge_type: [:online, :local]

  def pretty_location
    location.try(:formatted_location)
  end

  def pretty_address
    location.try(:formatted_address)
  end

  def formatted_times
    if end_date.present?
      "#{I18n.l(end_date, format: :pretty)}"
    elsif start_time.to_date == end_time.to_date
      "#{I18n.l(start_time, format: :short_with_date)} - #{I18n.l(end_time, format: :just_time)}"
    else
      "#{I18n.l(start_time, format: :short_with_date)} - #{I18n.l(end_time, format: :short_with_date)}"
    end
  end
end
