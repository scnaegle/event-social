class FlaggedItem < ActiveRecord::Base
  belongs_to :item, polymorphic: true
  belongs_to :user
  belongs_to :moderator, class_name: User

  validates :item_id, presence: true
  validates :item_type, presence: true
  validates :user_id, uniqueness: {scope: [:item_id, :item_type], message: "You cannot flag the same item more than once"}, presence: true
  validates :reason, presence: true

  after_commit :set_user_flagged_count, :set_item_flagged_count

  private
  
  def set_user_flagged_count
    item_creator = item.user
    item_creator.update(item_flagged_count: item_creator.item_flagged_count + 1) if item_creator.present?
    user.update(flagged_count: user.flagged_count + 1) if user.present?
  end

  def set_item_flagged_count
    item.update(flagged_count: item.flagged_count + 1) if item.present?
  end
end
