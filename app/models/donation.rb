class Donation < ActiveRecord::Base
  belongs_to :user
  belongs_to :event
  belongs_to :challenge
  has_many :charity_donations
  has_many :charities, through: :charity_donations
end
