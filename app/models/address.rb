class Address < ActiveRecord::Base
  belongs_to :addressable, polymorphic: true

  validates :line1, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip, presence: true

  def formatted_location
    lines = []
    lines << name if name.present?
    lines << formatted_address
    lines.join("\n")
  end

  def formatted_address
    lines = []
    lines << line1
    lines << line2 if line2.present?
    lines << "#{city}, #{state} #{zip}"
    lines << country if country.present?
    lines.join("\n")
  end
end
