class Event < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :suggestions
  has_many :donations
  has_many :donated_users, through: :donations, source: :user
  has_many :flags, as: :item, class_name: FlaggedItem
end
