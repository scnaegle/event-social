class CommentSuggestion < ActiveRecord::Base
  self.table_name = "comments"

  belongs_to :user
  belongs_to :challenge
  has_many :flags, as: :item, class_name: FlaggedItem
end
