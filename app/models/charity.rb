class Charity < ActiveRecord::Base
  has_many :charity_donations
  has_many :donations, through: :charity_donations
  has_one :address, as: :addressable, class_name: Address
end
