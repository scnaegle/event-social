class FlaggedItemsController < ApplicationController
  before_action :set_flagged_item, only: [:show, :edit, :update, :destroy]

  # GET /flagged_items
  # GET /flagged_items.json
  def index
    @flagged_items = FlaggedItem.all
  end

  # GET /flagged_items/1
  # GET /flagged_items/1.json
  def show
  end

  # GET /flagged_items/new
  def new
    @flagged_item = FlaggedItem.new(item_id: params[:item_id], item_type: params[:type])
  end

  # GET /flagged_items/1/edit
  def edit
  end

  # POST /flagged_items
  # POST /flagged_items.json
  def create
    @flagged_item = FlaggedItem.new(flagged_item_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @flagged_item.save
        format.html { redirect_to @flagged_item, notice: 'Flagged item was successfully created.' }
        format.json { render :show, status: :created, location: @flagged_item }
      else
        format.html { render :new }
        format.json { render json: @flagged_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /flagged_items/1
  # PATCH/PUT /flagged_items/1.json
  def update
    respond_to do |format|
      if @flagged_item.update(flagged_item_params)
        format.html { redirect_to @flagged_item, notice: 'Flagged item was successfully updated.' }
        format.json { render :show, status: :ok, location: @flagged_item }
      else
        format.html { render :edit }
        format.json { render json: @flagged_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /flagged_items/1
  # DELETE /flagged_items/1.json
  def destroy
    @flagged_item.destroy
    respond_to do |format|
      format.html { redirect_to flagged_items_url, notice: 'Flagged item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flagged_item
      @flagged_item = FlaggedItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def flagged_item_params
      params.require(:flagged_item).permit(:item_id, :item_type, :reason, :user_id, :moderator_id, :flagged)
    end
end
