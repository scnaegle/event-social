class AddTimesAndTypeToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :start_time, :datetime
    add_column :challenges, :end_time, :datetime
    add_column :challenges, :challenge_type, :integer, default: 0
  end
end
