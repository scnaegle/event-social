class CreateFlaggedItems < ActiveRecord::Migration
  def change
    create_table :flagged_items do |t|
      t.references :item, polymorphic: true
      t.text :reason
      t.integer :user_id
      t.integer :moderator_id
      t.boolean :flagged

      t.timestamps
    end
  end
end
