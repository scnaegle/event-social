class AddFlaggedCountToCommentsAndChallengesAndEvents < ActiveRecord::Migration
  def change
    add_column :comments, :flagged_count, :integer, default: 0
    add_column :events, :flagged_count, :integer, default: 0
    add_column :challenges, :flagged_count, :integer, default: 0
  end
end
