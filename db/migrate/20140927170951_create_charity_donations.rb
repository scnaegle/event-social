class CreateCharityDonations < ActiveRecord::Migration
  def change
    create_table :charity_donations do |t|
      t.integer :charity_id
      t.integer :donation_id
      t.float :percentage

      t.timestamps
    end
  end
end
