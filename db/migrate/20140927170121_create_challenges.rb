class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :title
      t.text :description
      t.date :end_date
      t.integer :user_id

      t.timestamps
    end
  end
end
