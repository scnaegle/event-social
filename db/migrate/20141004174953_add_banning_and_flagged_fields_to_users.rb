class AddBanningAndFlaggedFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :banned, :boolean, default: false
    add_column :users, :banned_count, :integer, default: 0
    add_column :users, :banned_limit, :datetime
    add_column :users, :flagged_count, :integer, default: 0
    add_column :users, :item_flagged_count, :integer, default: 0
  end
end
