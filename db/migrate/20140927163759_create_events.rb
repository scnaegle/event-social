class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.integer :user_id
      t.boolean :cancelled_flag, default: false
      t.boolean :completed_flag, default: false

      t.timestamps
    end
  end
end
