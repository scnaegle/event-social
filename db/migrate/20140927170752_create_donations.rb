class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.integer :challenge_id
      t.integer :user_id
      t.integer :event_id
      t.float :donation_amount

      t.timestamps
    end
  end
end
