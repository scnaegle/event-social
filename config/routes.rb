Rails.application.routes.draw do

  resources :flagged_items

  resources :charities

  resources :challenges do
    resources :donations
    resources :submissions
    resources :comments
  end

  resources :events do
    resources :donations
    resources :comments
  end

  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup
  root to: 'visitors#index'
end
