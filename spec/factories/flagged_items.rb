# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :flagged_item do
    item_id 1
    item_type "MyString"
    reason "MyText"
    user_id 1
    moderator_id 1
    flagged false
  end
end
