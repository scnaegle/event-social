# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :donation do
    challenge_id 1
    user_id 1
    event_id 1
    donation_amount 1.5
  end
end
