# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    title "MyString"
    description "MyText"
    start_date "2014-09-27 10:37:59"
    end_date "2014-09-27 10:37:59"
    user_id 1
    cancelled_flag false
    completed_flag false
  end
end
