# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :charity do
    name "MyString"
    description "MyText"
    website "MyString"
  end
end
