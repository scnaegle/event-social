# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :challenge do
    title "MyString"
    description "MyText"
    end_date "2014-09-27"
    user_id 1
  end
end
