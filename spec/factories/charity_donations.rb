# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :charity_donation do
    charity_id 1
    donation_id 1
    percentage 1.5
  end
end
